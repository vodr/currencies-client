let Container = mui.react.Container;

var CurrenciesBox = React.createClass({
	
	getInitialState: function() {
		return {
			currencies: [], 
			rates: {},
			amount: this.props.amount,
			base: this.props.base,
			filter: this.props.filter,
			mostPopular: null
		};
	},

	componentDidMount: function() {
		socket.on('newMostPopular', this.setMostPopular);
		this.loadCurrencies();
	},

	setMostPopular: function(val){
		this.setState({mostPopular: val});
	},

	handleAmountChange: function(amount){
		this.setState({amount: amount});
	},

	handleBaseChange: function(base){
		this.loadRates(base);
	},

	handleFilterChange: function(filter){
		this.setState({filter: filter});
	},

	loadRates: function(base){
		$.get({
			url: this.props.url + "rates/" + base,
			cache: false,
			success: function(data) {
				this.setState({rates: data.rates, base: base});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.props.url, status, err.toString());
			}.bind(this)
		});		
	},

	loadCurrencies: function(){
		$.get({
			url: this.props.url + "currencies",
			cache: false,
			success: function(data) {
				this.setState({
					currencies: data.currencies,
					mostPopular: data.mostPopular
				});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.props.url, status, err.toString());
			}.bind(this)
		});		
	},

	render: function() {
		if(this.state.currencies.length === 0){
			return (<div>Loading...</div>)
		}else{
			return (
			<Container>
				Most popular: {this.state.mostPopular}
				<CurrenciesForm  
				amount={this.state.amount} 
				base={this.state.base} 
				currencies={this.state.currencies}
				onAmountChange={this.handleAmountChange}
				onBaseChange={this.handleBaseChange}
				/>
				
				<CurrenciesRates  
				amount={this.state.amount} 
				rates={this.state.rates} 
				base={this.state.base}
				filter={this.state.filter}
				onFilterChange={this.handleFilterChange}
				/>

			</Container>
			);
		}
		
	}
});

window.CurrenciesBox = CurrenciesBox