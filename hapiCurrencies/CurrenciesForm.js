let Input = mui.react.Input, 
Option = mui.react.Option,
 Select = mui.react.Select;


var CurrenciesForm = React.createClass({

	onAmountChange: function(e){
		var val = parseFloat(e.target.value);
		this.props.onAmountChange(val);
	},

	onBaseChange: function(e){
		this.props.onBaseChange(e);
	},

	makeOption: function(val) {
        return (<Option key={val} value={val} label={val} />);
    },

    makeOptions: function(){
    	var options = this.props.currencies.map(this.makeOption);
    	options.unshift((<Option value={''} key={'choose'} label={'Choose currency'}/>));
    	return options;
    },

	render: function() {
		
   		return (
		  <div>
		    	Amount: <input 
		    		type="number"
		    		min="1"
		    		value={this.props.amount}
		    		onChange={this.onAmountChange} 
		    		autoFocus={true} />

        		<Select defaultValue={this.props.base} onChange={this.onBaseChange}>
        			{this.makeOptions()}
        		</Select>
		
		  </div>
		);
	}
});

window.CurrenciesForm = CurrenciesForm