let Input = mui.react.Input;

var CurrenciesRates = React.createClass({

	onFilterChange: function(e){
		var val = e.target.value;
		this.props.onFilterChange(val);
	},

	render: function() {
		if(this.props.base){
			var rates = [];
			var f = this.props.filter.toUpperCase();
			for(var i in this.props.rates){
				if(f === "" || i.indexOf(f) >= 0)
				rates.push((
					<div key={i}>{i}: {this.props.rates[i]*this.props.amount} </div>
					))
					
			}

		    return (
		      <div>
		      <Input 
		      		label={'Filter...'}
		    		floatingLabel={true}
		    		value={this.props.filter}
		    		onChange={this.onFilterChange}/> 
		      {rates}
		      </div>
		       
		    );
		}else{
			return (<div>No money, no results.</div>)
		}
		
	}
});

window.CurrenciesRates = CurrenciesRates